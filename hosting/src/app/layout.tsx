import type { Metadata } from "next";

import "../styles/globals.css";
import "../styles/base.css";
import "../styles/vendor.css";


export const metadata: Metadata = {
  title: "Jason D'Amour",
  description: "Jason D'Amour's website"
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      {children}
    </html>
  );
}

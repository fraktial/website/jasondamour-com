import Header from '@/components/Header';
import Intro from '@/components/Intro';
import About from '@/components/About';
import Resume from '@/components/Resume';
import Portfolio from '@/components/Portfolio';
import Services from '@/components/Services';
import Stats from '@/components/Stats';
import Contact from '@/components/Contact';
import Footer from '@/components/Footer';
import Construction from '@/components/Construction';

export default function Body() {
  return (
    <body id="top">
      <Header />
      <Construction />
      <Intro />
      <About />
      <Resume />
      <Portfolio />
      <Services />
      <Stats />
      <Contact />
      <Footer />
      <script src="js/jquery-2.1.3.min.js" async ></script>
      <script src="js/plugins.js" async ></script>
      <script src="js/main.js" async ></script>
    </body>
  );
}

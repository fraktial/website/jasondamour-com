import React from "react";
  
const Construction = () => {
  return (
    <div className="construction row">
      <h1>Pardon my dust!</h1>
      <h3>I&apos;m rewriting my site from plain HTML to React!</h3>
    </div>
  );
};

export default Construction;

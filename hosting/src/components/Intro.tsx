import React from "react";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faDiscord, faGithub, faGitlab, faLinkedin } from '@fortawesome/free-brands-svg-icons'

const Intro = () => {
  return (
	<section id="intro">

		<div className="intro-overlay"></div>

		<div className="intro-content">
			<div className="row">

				<div className="col-twelve">

					<h5>Hello World,</h5>
					<h1>I&apos;m Jason D&apos;Amour.</h1>

					<p className="intro-position">
						<span>Engineer</span>
						<span>Adventurer</span>
						<span>Gamer</span>
					</p>
					<div className="intro-content-button">
						<a className="button stroke smoothscroll" href="#about" title="">Who Am I?</a>
					</div>
				</div>

			</div>
		</div>

		<ul className="intro-social">
			<li><a href="https://www.linkedin.com/in/jdamour/"><FontAwesomeIcon icon={faLinkedin} size="lg" /></a></li>
			<li><a href="https://gitlab.com/jason.damour"><FontAwesomeIcon icon={faGitlab} size="lg" /></a></li>
			<li><a href="https://github.com/jasondamour"><FontAwesomeIcon icon={faGithub} size="lg" /></a></li>
			<li><a href="https://discord.com/users/379672044498976780"><FontAwesomeIcon icon={faDiscord} size="lg" /></a></li>
		</ul>

	</section>
  );
};

export default Intro;

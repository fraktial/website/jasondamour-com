"use client";

import { Tooltip } from "react-tooltip";

import Image from "next/image";
import "react-calendar-heatmap/dist/styles.css";
import pfp from "./images/pfp.jpg";

const About = () => {
  // const gitContribs = fetch(
  //   "https://us-central1-jasondamour.cloudfunctions.net/git_contributions"
  // ).then((res) => res.json());
  return (
    <section id="about">
      <div className="row section-intro">
        <div className="col-twelve">
          <h5>About</h5>
          <h1>Let me introduce myself.</h1>
          <div className="intro-info">
            <Image src={pfp} alt="Profile Picture" width={100} height={100}/>
            <p className="lead">
              Coding, climbing, diving, surfing, racing, gaming, snowboarding,
              spelunking, and exploring the world.{" "}
            </p>
          </div>
        </div>
      </div>

      <div className="row about-content">
        <div className="col-six tab-full">
          <h3>At Home</h3>
          <p>
            Born and raised in the Bay Area, I&apos;ve been calling San
            Francisco home since 2020. When the weather is warm I enjoy surfing
            in Santa Cruz, diving in Monterey, and hiking in Yosemite. When the
            weather is cold I love snowboarding, whether in Tahoe or around the
            world. I love day trips in my car, long road trips in converted
            camper vans, and going nowhere at all. When I&apos;m not out and
            about, I enjoy playing PC games like Rocket League, Baldurs Gate 3,
            or Apex Legends.
          </p>
          <ul className="info-list">
            <li>
              <strong>Name:</strong>
              <span>Jason D&apos;Amour</span>
            </li>
            <li>
              <strong>Born:</strong>
              <span>August, 1998</span>
            </li>
            <li>
              <strong>Job:</strong>
              <span>Software Engineer</span>
            </li>
            <li>
              <strong>Email:</strong>
              <span>jasondamour98@gmail.com</span>
            </li>
          </ul>
        </div>
        <div className="col-six tab-full">
          <h3>At Work</h3>
          <p>
            As a Computer Science major at Colorado State University, I became
            involved with an engineering research team. I helped run and
            maintain the on-prem Kubernetes cluster of ~100 nodes; from hardware
            upgrades to bootstraping clusters with kubeadm. After pinching my
            thumb on a server rack, I decided to retire from the ground work and
            become a cloud engineer. Happily FUNemployed.
          </p>

          <h3>Skills</h3>

          <ul className="skill-bars">
            <li>
              <div className="progress percent90">
                <span>90%</span>
              </div>
              <strong>Snowboarding</strong>
            </li>
            <li>
              <div className="progress percent85">
                <span>85%</span>
              </div>
              <strong>Climbing</strong>
            </li>
            <li>
              <div className="progress percent75">
                <span>75%</span>
              </div>
              <strong>Diving</strong>
            </li>
            <li>
              <div className="progress percent45">
                <span>45%</span>
              </div>
              <strong>Cooking</strong>
            </li>
            <li>
              <div className="progress percent50">
                <span>50%</span>
              </div>
              <strong>Surfing</strong>
            </li>
          </ul>
        </div>
      </div>

      {/* <div className="row about-content">
        <CalendarHeatmap
          startDate={new Date().setFullYear(new Date().getFullYear() - 1)}
          values={gitContribs.contributions}
					tooltipDataAttrs={(value: { date: string, count: number }) => {
						return {
							'data-tooltip-content': `${value.date} has count: ${value.count}`,
						};
					}}
        />
      </div> */}

      <div className="row button-section">
        <div className="col-twelve">
          <a
            href="#contact"
            title="Contact"
            className="button stroke smoothscroll"
          >
            Contact
          </a>
        </div>
      </div>
      <Tooltip />
    </section>
  );
};

export default About;

import React from "react";

const Contact = () => {
  return (
    <section id="contact">

		<div className="row section-intro">
			<div className="col-twelve">

				<h5>Contact</h5>
				<h1>I would Love To Hear From You.</h1>

				<p className="lead">
					If you made it all the way down here, then a sincere &quot;Thank You&quot; for taking such an interest. If
					you would like to
					know more, please reach out. The best way to contact me is through LinkedIn, but I will do my best to
					respond
					via any channel.
				</p>

			</div>
		</div>

		<div className="row contact-info">

			<div className="col-four tab-full">
				<div className="icon">
					<i className="icon-pin"></i>
				</div>
				<h5>Where to find me</h5>
				<p>
					San Francisco Bay Area, CA<br/>
					94110 US
				</p>
			</div>

			<div className="col-four tab-full collapse">
				<div className="icon">
					<i className="icon-mail"></i>
				</div>
				<h5>Email Me At</h5>
				<p>jasondamour98@gmail.com</p>
			</div>

			<div className="col-four tab-full">
				<div className="icon">
					<i className="icon-phone"></i>
				</div>
				<h5>Call Me At</h5>
				<p>Don&apos;t call me.</p>
				<p>Seriously.</p>
			</div>

		</div>

	</section>
  );
};

export default Contact;

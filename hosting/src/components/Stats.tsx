import React from "react";

const Stats = () => {
  return (
    <section id="stats" className="count-up">
      <div className="row">
        <div className="col-twelve">
          <div className="block-1-6 block-s-1-3 block-tab-1-2 block-mob-full stats-list">
            <div className="bgrid stat">
              <div className="icon-part">
                <i className="icon-settings"></i>
              </div>
              <h3 className="stat-count">1200000+</h3>
              <h5 className="stat-title">CI/CD jobs</h5>
            </div>

            <div className="bgrid stat">
              <div className="icon-part">
                <i className="icon-terminal"></i>
              </div>
              <h3 className="stat-count">8000+</h3>
              <h5 className="stat-title">Production Deployments</h5>
            </div>

            <div className="bgrid stat">
              <div className="icon-part">
                <i className="icon-cup"></i>
              </div>
              <h3 className="stat-count">3000+</h3>
              <h5 className="stat-title">Coffees</h5>
            </div>

            <div className="bgrid stat">
              <div className="icon-part">
                <i className="icon-cloud"></i>
              </div>
              <h3 className="stat-count">50+</h3>
              <h5 className="stat-title">Kubernetes Clusters</h5>
            </div>

            <div className="bgrid stat">
              <div className="icon-part">
                <i className="icon-badge"></i>
              </div>
              <h3 className="stat-count">4</h3>
              <h5 className="stat-title">Cloud Certifications</h5>
            </div>

            <div className="bgrid stat">
              <div className="icon-part">
                <i className="icon-wrench"></i>
              </div>
              <h3 className="stat-count">0</h3>
              <h5 className="stat-title">Hours Downtime</h5>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Stats;

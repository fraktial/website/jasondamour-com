import React from "react";

const Services = () => {
  return (
    <section id="services">
      <div className="overlay"></div>

      <div className="row section-intro">
        <div className="col-twelve">
          <h5>Services</h5>
          <h1>What Can I Do For You?</h1>

          <p className="lead">
            I am creative, passionate developer. I can make logical decisions
            independently, and communicate effectively to ensure I&apos;m on the same
            page as everyone else. I can take leadership on projects, guide
            peers, and always KISS (Keep it Simple Stupid)!
          </p>
        </div>
      </div>

      <div className="row services-content">
        <div id="owl-slider" className="owl-carousel services-list">
          <div className="service">
            <span className="icon">
              <i className="icon-window"></i>
            </span>
            <div className="service-content">
              <h3>Web Hosting</h3>
              <p className="desc">
                If you like my website, I can help you build one for yourself
                too! Although frontend development is not my speciality, I love
                the challenge and creativity. I especially enjoy working with
                the latest technologies and frameworks. I take pride in not just
                delivering a static product, but the framework and automation
                tools to continue to evolve websites over time.
              </p>
            </div>
          </div>

          <div className="service">
            <span className="icon">
              <i className="icon-pencil"></i>
            </span>
            <div className="service-content">
              <h3>Tutoring</h3>
              <p className="desc">
                I have multiple years of experience with Google Cloud Platform
                in production at large-scale. If you are looking to learn more
                about it yourself, I will gladly explain concepts and guide you
                through hands-on exercises. I am also well versed on Java,
                NodeJS, and Python.
              </p>
            </div>
          </div>

          <div className="service">
            <span className="icon">
              <i className="icon-chat"></i>
            </span>
            <div className="service-content">
              <h3>Consultancy</h3>
              <p className="desc">
                I have 5+ years of professional experience building advanced
                infrastructure systems and processes, and have boostrapped two
                organizations from nothing to industry leading release
                lifecycles. I can bring those best practices to you!
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Services;

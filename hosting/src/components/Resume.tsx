import React from "react";

const Resume = () => {
  return (
    <section id="resume" className="grey-section">
      <div className="row section-intro">
        <div className="col-twelve">
          <h5>Resume</h5>
          <h1>Here is what I have accomplished.</h1>
          <p className="lead">
            <a href="resouces/Jason-DAmour-Resume.pdf">
              Download my resume here
            </a>
            or check it out on
            <a href="https://www.linkedin.com/in/jdamour">LinkedIn</a>
          </p>
        </div>
      </div>

      <div className="row resume-timeline">
        <div className="col-twelve resume-header">
          <h2>Experience</h2>
        </div>

        <div className="col-twelve">
          <div className="timeline-wrap">
            <div className="timeline-block">
              <div className="timeline-ico">
                <i className="fa fa-briefcase"></i>
              </div>
              <div className="timeline-header">
                <h3>Staff</h3>
                <h3>Software Engineer</h3>
                <p>July 2023 - Present</p>
              </div>
              <div className="timeline-content">
                <h4>Five9</h4>
                <ul>
                  <li>Wrote technical requirements for a team of engineers</li>
                  <li>
                    Directly oversaw an intern with daily check-ins and close
                    oversight
                  </li>
                  <li>
                    Oversaw migration of a critical core component from our
                    on-prem data center to our cloud native architecture
                  </li>
                  <li>
                    Onboarded the entire stack of an acquired company, ending
                    with their on-prem stack running as single-tenant namespaces
                    in GKE, tied to our automated provisioning flow
                  </li>
                </ul>
              </div>
            </div>

            <div className="timeline-block">
              <div className="timeline-ico">
                <i className="fa fa-briefcase"></i>
              </div>
              <div className="timeline-header">
                <h3>Senior</h3>
                <h3>Software Engineer</h3>
                <p>Feburary 2021 - July 2023</p>
              </div>
              <div className="timeline-content">
                <h4>Five9</h4>
                <ul>
                  <li>Managed migration of POPs from one region to another</li>
                  <li>
                    Designed the architecture of our telecom system on
                    Kubernetes
                  </li>
                  <li>
                    Oversaw aggressive cost saving efforts, cutting more than
                  </li>
                  60% of operation cost, and automated cost monitoring
                  <li>
                    Established SLAs/SLIs in collaboration with our SRE team
                  </li>
                  <li>
                    Integrated a slew of security tools for dependency
                    vulnerability, license management, and code quality
                  </li>
                </ul>
              </div>
            </div>

            <div className="timeline-block">
              <div className="timeline-ico">
                <i className="fa fa-briefcase"></i>
              </div>
              <div className="timeline-header">
                <h3>Software Engineer</h3>
                <p>May 2018 - Feburary 2021</p>
              </div>
              <div className="timeline-content">
                <h4>Five9</h4>
                <ul>
                  <li>
                    Onboarded microservices onto a new cloud native platform
                  </li>
                  <li>
                    Set the standard directory structure of Git repos, gradle
                    modules, helm charts, dockerfiles, and so forth
                  </li>
                  <li>
                    Implemented the centralized repository for CI/CD templates,
                    using a modular pattern which allowed flexible usage
                  </li>
                  <li>
                    Deployed self-hosted CI Runners and did extensive
                    performance tuning
                  </li>
                </ul>
              </div>
            </div>

            <div className="timeline-block">
              <div className="timeline-ico">
                <i className="fa fa-briefcase"></i>
              </div>
              <div className="timeline-header">
                <h3>Devops Intern at Engineering Research</h3>
                <p>August 2017 - May 2018</p>
              </div>
              <div className="timeline-content">
                <h4>Colorado State University</h4>
                <ul>
                  <li>
                    Maintenance and deployment of On-Prem Kubernetes Cluster
                  </li>
                  <li>Assembled, wired, and disassembled servers</li>
                </ul>
              </div>
            </div>

            <div className="timeline-block">
              <div className="timeline-ico">
                <i className="fa fa-briefcase"></i>
              </div>
              <div className="timeline-header">
                <h3>Software Development Intern</h3>
                <p>May 2017 - August 2017</p>
              </div>
              <div className="timeline-content">
                <h4>Five9</h4>
                <p>
                  Primarily assisted with the development of a SMS Routing
                  Service, as a POC for expanding Omnichannel Customer Service.
                  Studied Springboot principals, improved code quality, and
                  modified SQL database.
                </p>
              </div>
            </div>

            <div className="timeline-block">
              <div className="timeline-ico">
                <i className="fa fa-briefcase"></i>
              </div>
              <div className="timeline-header">
                <h3>Computer Science Department Tutor</h3>
                <p>September 2016 - May 2017</p>
              </div>
              <div className="timeline-content">
                <h4>Colorado State University</h4>
                <p>
                  Colorado State University provides free tutors to students in
                  the department. I was a tutor as a freshman which is note-able
                  because not many freshman had the required course credits,
                  which I acquired through AP classNamees in high school
                </p>
              </div>
            </div>

            <div className="timeline-block">
              <div className="timeline-ico">
                <i className="fa fa-briefcase"></i>
              </div>
              <div className="timeline-header">
                <h3>Assistant Instructor of Programming</h3>
                <p>May 2016 - August 2017</p>
              </div>
              <div className="timeline-content">
                <h4>Galileo Learning</h4>
                <p>
                  Conducted week-long classNamees for middle school age groups
                  titled
                  <a href="https://galileo-camps.com/our-camps/summer-camp-galileo/">
                    &quot;Mod Design with Minecraft&quot;
                  </a>
                  Groups of 14-16 students follow a strong curriculum teaching
                  simple Java skills, allowing students to add custom game
                  elements, actions, and designs.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="row resume-timeline">
        <div className="col-twelve resume-header">
          <h2>Education</h2>
        </div>

        <div className="col-twelve">
          <div className="timeline-wrap">
            <div className="timeline-block">
              <div className="timeline-ico">
                <i className="fa fa-graduation-cap"></i>
              </div>
              <div className="timeline-header">
                <h3>Bachelors degree</h3>
                <p>August 2016 - May 2018</p>
              </div>
              <div className="timeline-content">
                <h4>Colorado State University</h4>
                <p>B.S. of Computer Science [On Hold]</p>
              </div>
            </div>
          </div>
        </div>
        <div className="row button-section">
          <div className="col-twelve">
            <a
              href="resouces/Resume-Jason-D'Amour.pdf"
              title="Download Resume"
              className="button button-primary"
            >
              Download Resume
            </a>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Resume;

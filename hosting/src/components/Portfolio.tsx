import Image from "next/image";
import React from "react";

const Portfolio = () => {
  return (
    <section id="portfolio">
      <div className="row section-intro">
        <div className="col-twelve">
          <h5>Portfolio</h5>
          <h1>Check out some of my personal projects</h1>
          <p className="lead">
            This is a collection of personal open-source projects, available for
            use or hosted right here!
          </p>
        </div>
      </div>

      <div className="row portfolio-content">
        <div className="col-twelve">
          <div id="folio-wrapper" className="block-1-2 block-mob-full stack">
            <div className="bgrid folio-item">
              <div className="item-wrap">
                <Image src="/images/portfolio/discordbot.png" alt="discordbot" width={100} height={100} />
                <a href="#discord-bot-modal" className="overlay">
                  <div className="folio-item-table">
                    <div className="folio-item-cell">
                      <h3 className="folio-title">Discord Gcloud Bot</h3>
                      <span className="folio-types">
                        A nodejs project based on slash-commands library for
                        discord interactions
                      </span>
                    </div>
                  </div>
                </a>
              </div>
            </div>
            <div id="discord-bot-modal" className="popup-modal slider mfp-hide">
              <div className="media">
                <Image src="/images/portfolio/discordbot.png" alt="" width={100} height={100} />
              </div>
              <div className="description-box">
                <h4>Google Cloud Discord Bot</h4>
                <p>
                  This project allows my friends in Discord with little-to-none
                  computer skills interact with gaming servers which I setup and
                  maintain in Google Cloud. Users can use the interactive
                  slash-commands to understand how to start, stop, list, and
                  connect to the servers.
                </p>
                <div className="categories">NodeJS, GCP Functions, </div>
              </div>
              <div className="link-box">
                <a href="https://github.com/jasondamour/discord-gcloud-commands">
                  GitHub
                </a>
                <a href="#" className="popup-modal-dismiss">
                  Close
                </a>
              </div>
            </div>

            <div className="bgrid folio-item">
              <div className="item-wrap">
                <Image src="/images/portfolio/globe.png" alt="website" width={100} height={100} />
                <a href="#weebsite" className="overlay">
                  <div className="folio-item-table">
                    <div className="folio-item-cell">
                      <h3 className="folio-title">This Website</h3>
                      <span className="folio-types">This Website</span>
                    </div>
                  </div>
                </a>
              </div>
            </div>
            <div id="weebsite" className="popup-modal slider mfp-hide">
              <div className="media">
                <Image src="/images/portfolio/globe.png" alt="" width={100} height={100} />
              </div>
              <div className="description-box">
                <h4>jasondamour.com</h4>
                <p>
                  Check out the source code of what you&apos;re looking at right now!
                </p>
                <div className="categories">HTML, CSS, NodeJS, Firebase</div>
              </div>
              <div className="link-box">
                <a href="https://gitlab.com/fraktial/website/jasondamour-com">
                  GitLab
                </a>
                <a href="#" className="popup-modal-dismiss">
                  Close
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Portfolio;

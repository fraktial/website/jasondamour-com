import React from "react";

const Footer = () => {
  return (
    <footer>
		<div className="row">

			<div className="col-six tab-full pull-right social">

				<ul className="footer-social">
					<li><a href="https://www.linkedin.com/in/jdamour/"><i className="fab fa-lg fa-linkedin"></i></a></li>
					<li><a href="https://gitlab.com/jason.damour"><i className="fab fa-lg fa-gitlab"></i></a></li>
					<li><a href="https://github.com/jasondamour"><i className="fab fa-lg fa-github"></i></a></li>
					<li><a href="https://discord.com/users/379672044498976780"><i className="fab fa-lg fa-discord"></i></a>
					</li>
				</ul>

			</div>

			<div id="go-top">
				<a className="smoothscroll" title="Back to Top" href="#top"><i className="fa fa-arrow-up"></i></a>
			</div>

		</div>
	</footer>
  );
};

export default Footer;

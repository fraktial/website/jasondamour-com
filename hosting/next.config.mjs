/** @type {import('next').NextConfig} */
const nextConfig = {
    env: {
        gitContributionAPI: 'https://us-central1-jasondamour.cloudfunctions.net/git_contributions',
    }
};

export default nextConfig;

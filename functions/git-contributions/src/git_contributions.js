const logger = require("firebase-functions/logger");

const {
  githubContributionData,
  gitlabContributionData,
} = require("./requests");

async function gitContributions(req, res) {
  console.log(req.query);
  var githubUsernames, gitlabUsernames;
  if (req.query.gh == null && req.query.gl == null) {
    githubUsernames = ["jasondamour"];
    gitlabUsernames = ["jason.damour"];
    // 3870
  } else {
    githubUsernames =
      typeof req.query.gh === "string" ? [req.query.gh] : req.query.gh;
    gitlabUsernames =
      typeof req.query.gl === "string" ? [req.query.gl] : req.query.gl;
  }

  logger.info(
    `Fetching data for Github=${githubUsernames}, Gitlab=${gitlabUsernames}`
  );

  /**
   * Example:
   * contributions: [
   *    {
   *       date: '2021-9-22',
   *       count: 5
   *    }
   * ]
   */
  let contributions = [];

  if (githubUsernames != null) {
    for (const username of githubUsernames) {
      await githubContributionData(username).then((data) => {
        data.forEach((week) => {
          week.contributionDays.forEach((day) => {
            contributions.push({
              date: day.date,
              count: day.contributionCount,
            });
          });
        });
      });
    }
  }

  if (gitlabUsernames != null) {
    for (const username of gitlabUsernames) {
      await gitlabContributionData(username).then((data) => {
        contributions = contributions.concat(
          Object.entries(data).map(([key, value]) => ({
            date: key,
            count: value,
          }))
        );
      });
    }
  }

  // Deduplicate dates, adding the counts
  contributions = Object.values(
    contributions.reduce((contribMap, { date, count }) => {
      contribMap[date] = {
        date,
        count: (contribMap[date]?.count || 0) + count,
      };
      return contribMap;
    }, {})
  );

  // Calculate the total contributions
  const totalContributionCount = contributions.reduce(
    (accumulator, contribution) => {
      return accumulator + contribution.count;
    },
    0
  );

  res.status(200).json({
    totalContributionCount,
    contributions,
  });
}

module.exports = {
  gitContributions,
};

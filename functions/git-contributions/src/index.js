const { onRequest } = require("firebase-functions/v2/https");
const { gitContributions } = require("./git_contributions");

exports.git_contributions = onRequest(
  {
    cors: [
      "jasondamour.com",
      "amritav.com",
      /jasondamour.cloudfunctions.net$/,
      /localhost/,
    ],
    secrets: ["GIT_CONTRIBUTIONS_GITHUB_TOKEN"],
  },
  gitContributions
);
